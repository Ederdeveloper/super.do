//
//  WebSocketManager.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON

// MARK: - constants
let baseURL = "ws://superdo-groceries.herokuapp.com/receive"

final class WebSocketManager: WebSocketDelegate {

    // Singltone instance
    static let shared = WebSocketManager()
    
    // available WebSockets
    var vegetableUrl: URL?
    var vegetableWebSocket: WebSocket?
    var vegetableBlock: ((_ vegetable: Vegetable) -> Void)?
    
    private init() {}
    
    // MARK: - WebSocket connection
    // ============================
    // another webSockets could be added here if nedded.
    func connectToVegetableSocket(_ block: @escaping (_ vegetable: Vegetable) -> Void) {
        self.vegetableBlock = block
        self.vegetableUrl = URL(string: baseURL)!
        self.vegetableWebSocket = WebSocket(url: self.vegetableUrl!, protocols: ["websocket"])
        vegetableWebSocket?.delegate = self
        self.vegetableWebSocket?.connect()
    }
    
    func disconnectToVegetableSocket() {
        self.vegetableWebSocket?.disconnect()
    }
    
    // MARK: - webSocket delegate
    // ==========================
    public func websocketDidConnect(socket: Starscream.WebSocket) {
        print("websocketDidConnect successfully!")
    }
    
    public func websocketDidDisconnect(socket: Starscream.WebSocket, error: NSError?) {
        print("websocketDidDisconnect")
    }
    
    public func websocketDidReceiveMessage(socket: Starscream.WebSocket, text: String) {
        if socket == vegetableWebSocket {
            //  parse json from a string:
            if let json = text.SwiftyJson {
                let vegetable = Vegetable(json: json)
                self.vegetableBlock?(vegetable)
            }
        }
    }
    
    func websocketDidReceiveData(socket: WebSocket, data: Data) {
        print("websocketDidReceiveData \(data)")
    }
    
}
