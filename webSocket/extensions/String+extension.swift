//
//  String+extension.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import Foundation
import SwiftyJSON

extension String {
    var SwiftyJson : JSON? {
        if let data = self.data(using: String.Encoding.utf8) {
            let json = JSON(data)
            return json
        }
        return nil
    }
}
