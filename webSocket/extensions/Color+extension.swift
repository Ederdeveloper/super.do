//
//  Color+extension.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init?(hex: String) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            self.init(white: 0.0, alpha: 1.0)
            return
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: 1.0)
    }
    
    static var lightGrayBorder: UIColor {
        return UIColor(hex: "dddddd")!
    }
    
    static var darkGrayBorder: UIColor {
        return UIColor(hex: "888888")!
    }
    
    static var lightGrayBackground: UIColor {
        return UIColor(hex: "f5f5f5")!
    }
}
