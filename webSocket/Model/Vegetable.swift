//
//  Vegetable.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import UIKit
import SwiftyJSON

class Vegetable {
    var name: String?
    var weight: String?
    var bgColor: String?
    
    init(json: JSON) {
        self.bgColor = json["bagColor"].string
        self.name = json["name"].string
        self.weight = json["weight"].string
    }
}
