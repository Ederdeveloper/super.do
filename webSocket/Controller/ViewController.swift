//
//  ViewController.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import UIKit

enum LayoutState {
    case Table
    case Collection
}

// MARK: - constants
let vegetableTableCell = "vegetableTableCell"
let vegetableTableCellHeight: CGFloat = 113.0

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    // MARK: - outlets
    // ===============
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - variables
    // =================
    var vegetables = [Vegetable]()
    var layoutState: LayoutState! {
        didSet {
            self.tableView.isHidden = self.layoutState == .Collection
            self.tableView.reloadData()
            self.collectionView.isHidden = self.layoutState == .Table
            self.collectionView.reloadData()
        }
    }

    // MARK: - ViewController functions
    // ================================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData()
        self.layoutState = LayoutState.Table
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func loadData() {
        WebSocketManager.shared.connectToVegetableSocket { (vegetable) in
            DispatchQueue.main.async {
                self.vegetables.insert(vegetable, at: 0)
                let indexPath = IndexPath(row: 0, section: 0)
                if self.layoutState == .Table {
                    self.tableView.beginUpdates()
                    self.tableView.insertRows(at: [indexPath], with: .middle)
                    self.tableView.endUpdates()
                } else {
                    self.collectionView.insertItems(at: [indexPath])
                }
            }
        }
    }
    
    // MARK: - TableView Functions
    // ===========================
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if layoutState == .Table {
            return self.vegetables.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: vegetableTableCell, for: indexPath) as! VegetableTableViewCell
        cell.setVegetable(self.vegetables[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return vegetableTableCellHeight
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
    }
    
    // MARK: - CollectionView Functions
    // ================================
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if layoutState == .Collection {
            return self.vegetables.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VegetableCollectionViewCell", for: indexPath) as! VegetableCollectionViewCell
        cell.setVegetable(self.vegetables[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 113)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (coordinatorContext) in
            if size.height > size.width {
                self.layoutState = .Table
            } else {
                self.layoutState = .Collection
            }
        }) { (coordinatorContext) in
            
        }
    }
    
}

