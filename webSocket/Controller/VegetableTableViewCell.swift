//
//  VegetableTableViewCell.swift
//  webSocket
//
//  Created by Eder Yifrach on 2/19/18.
//  Copyright © 2018 Eder Yifrach. All rights reserved.
//

import UIKit

class VegetableTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    // =================
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    
    // MARK: - Cell functions
    // ======================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.updateUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Custom functions
    // ========================
    func updateUI() {
        self.colorView.layer.cornerRadius = self.colorView.frame.height/2
        self.colorView.layer.borderColor = UIColor.darkGrayBorder.cgColor
        self.colorView.layer.borderWidth = 2.0
        
        self.mainView.layer.cornerRadius = 4.0
        self.mainView.layer.borderColor = UIColor.lightGrayBorder.cgColor
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.masksToBounds = true
        
        self.nameView.backgroundColor = UIColor.lightGrayBackground
        self.nameView.layer.borderColor = UIColor.lightGrayBorder.cgColor
        self.nameView.layer.borderWidth = 1.0
    }
    
    func setVegetable(_ vegetable: Vegetable) {
        self.lblName.text = vegetable.name ?? ""
        self.lblWeight.text = vegetable.weight ?? ""
        self.colorView.backgroundColor = UIColor(hex: vegetable.bgColor ?? "")
    }

}
